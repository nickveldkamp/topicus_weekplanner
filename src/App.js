import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Style
import "./App.css";

// Pages
import Nav from "./Nav";
import AddEvent from "./AddEvent";
import data from "./Data";

//Fullcalendar and Realted Plugins
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction"; // needed
import timegridPlugin from "@fullcalendar/timegrid"; // needed

function App() {
  const [events, setEvents] = useState(data);

  // Event Render Function To Get Title and Name
  function renderEventContent(eventInfo) {
    return (
      <div>
        <h3>{eventInfo.event.title}</h3>
        <h4>{eventInfo.event.extendedProps.name}</h4>
      </div>
    );
  }

  return (
    <div className="App">
      <Router>
        {/* Navbar  */}
        <Nav></Nav>

        <Switch>
          {/* Home page */}
          <Route exact path="/">
            <div className="App__Calendar">
              <FullCalendar
                plugins={[timegridPlugin, dayGridPlugin, interactionPlugin]}
                initialView="timeGridWeek"
                eventContent={renderEventContent}
                selectOverlap={false}
                eventOverlap={false}
                slotEventOverlap={false}
                allDaySlot={false}
                displayEventTime={true}
                timeZone="local"
                locale="nl"
                weekends={false}
                events={events}
              />
            </div>
          </Route>

          {/* // Add event page */}
          <Route exact path="/addEvent" component={AddEvent}>
            <AddEvent events={events} setEvents={setEvents}></AddEvent>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
