import React, { useState } from "react";
import "./AddEvent.css";
import { v4 as uuidv4 } from "uuid";
import { useHistory } from "react-router-dom";
import Moment from "moment";
import { extendMoment } from "moment-range";

function AddEvent({ events, setEvents }) {
  const moment = extendMoment(Moment);
  const history = useHistory();
  const [ErrorState, setErrorState] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const submitEventHandler = (e) => {
    e.preventDefault();

    // check if form is valid and filled in
    if (!e.target.checkValidity()) {
      setErrorState(true);
      setErrorMessage("Vul alle gegevens in");
      return;
    }

    // form is valid! We can parse and submit the information to the calendar
    let inpTitle = e.target.eventName.value;
    let inpPatient = e.target.patient.value;
    let inpDate = e.target.date.value;
    let inpStartTime = e.target.startTime.value;
    let inpEndTime = e.target.endTime.value;

    let weekDayName = moment(inpDate).format("dddd");
    if (weekDayName === "Saturday" || weekDayName === "Sunday") {
      setErrorMessage(
        "Helaas, Een event kan niet worden geplaatst in het weekend. Kies voor een andere dag."
      );
      return;
    }
    // Fill items with information
    const eventItem = {
      id: uuidv4(),
      title: inpTitle,
      start: `${inpDate}T${inpStartTime}`,
      end: `${inpDate}T${inpEndTime}`,
      extendedProps: {
        name: inpPatient,
      },
    };

    // If event doesn't overlap > Add Event
    if (!isOverlapping(eventItem)) {
      setEvents([...events, eventItem]);

      // Clear inputfields
      inpTitle = "";
      inpPatient = "";
      inpDate = "";
      inpStartTime = "";
      inpEndTime = "";

      // Push back to Home (Calendar)
      history.push("/");
    } else {
      // If Event is overlapping show message
      setErrorMessage(
        "Event overlapt een ander event. Kies een ander tijdstip"
      );
    }
  };

  function isOverlapping(item) {
    const a = moment(item.start);
    const b = moment(item.end);
    let c = "";
    let d = "";
    let returnValue = false;

    events.map((eventItem) => {
      if (eventItem.dateValue === item.dateValue) {
        c = eventItem.start;
        d = eventItem.end;

        const range1 = moment.range(a, b);
        const range2 = moment.range(c, d);

        if (range1.overlaps(range2) === true) {
          returnValue = true;
        }
      }
    });
    return returnValue;
  }

  return (
    <div className="AddEvent">
      <h1>Event Toevoegen</h1>
      <h5 className="AddEvent__ErrorMessage">{errorMessage}</h5>
      <form
        onSubmit={submitEventHandler}
        className={ErrorState ? "AddEvent__ErrorState" : ""}
        noValidate
      >
        <h4>Titel:</h4>
        <input type="text" name="eventName" required />
        <h4>Patiënt:</h4>
        <input type="text" name="patient" required />
        <h4>Datum:</h4>
        <input
          type="date"
          name="date"
          data-parse="date"
          pattern="\d{2}\/\d{2}/\d{4}"
          required
        />
        <div className="AddEvent__TimeMain">
          <div className="time">
            <h6>Start tijd:</h6>
            <input type="time" name="startTime" required />
          </div>
          <div className="time">
            <h6>Eind tijd:</h6>
            <input type="time" name="endTime" required />
          </div>
        </div>
        <button>Toevoegen</button>
      </form>
    </div>
  );
}

export default AddEvent;
