import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarPlus, faHome } from "@fortawesome/free-solid-svg-icons";
import React from "react";
import { NavLink } from "react-router-dom";
import logo from "./images/logo.png";
import "./Nav.css";

function Nav() {
  const isActive = {
    fontWeight: "bold",
    backgroundColor: "rgba(255, 255, 255, 0.1)",
  };
  return (
    <div>
      <nav>
        <ul className="Nav__Main">
          <NavLink to="/">
            <img className="Nav__Logo" src={logo} height="70" alt="" />
          </NavLink>
          <li>
            <NavLink to="/" exact={true} activeStyle={isActive}>
              <FontAwesomeIcon icon={faHome} />
              &nbsp; Home
            </NavLink>
          </li>
          <li>
            <NavLink to="/addEvent" activeStyle={isActive}>
              <FontAwesomeIcon icon={faCalendarPlus} /> &nbsp; Event Toevoegen
            </NavLink>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default Nav;
