import { v4 as uuidv4 } from "uuid";

function data() {
  return [
    {
      id: uuidv4(),
      title: "Corona Onderzoek",
      start: "2020-11-13T11:00",
      end: "2020-11-13T13:00",
      extendedProps: {
        name: "Alet Haakma",
      },
    },
    {
      id: uuidv4(),
      title: "Cardioloog Onderzoek",
      start: "2020-11-13T16:00",
      end: "2020-11-13T17:00",
      extendedProps: {
        name: "Tyas Bergkamp",
      },
    },
    {
      id: uuidv4(),
      title: "Orthopedisch Onderzoek",
      start: "2020-11-12T10:00",
      end: "2020-11-12T11:15",
      extendedProps: {
        name: "Renier Scheffer",
      },
    },
    {
      id: uuidv4(),
      title: "Cosmetische chirurgie",
      start: "2020-11-14T12:00",
      end: "2020-11-14T13:15",
      extendedProps: {
        name: "İsmail Winkens",
      },
    },
    {
      id: uuidv4(),
      title: "Cosmetische chirurgie",
      start: "2020-11-15T15:00",
      end: "2020-11-15T16:00",
      extendedProps: {
        name: "Sabir van der Weg",
      },
    },
    {
      id: uuidv4(),
      title: "Interne geneeskunde",
      start: "2020-11-17T10:00",
      end: "2020-11-17T12:00",
      extendedProps: {
        name: "Jordan van der Stelt",
      },
    },
    {
      id: uuidv4(),
      title: "Infectieziekten",
      start: "2020-11-16T09:00",
      end: "2020-11-16T10:00",
      extendedProps: {
        name: "Meis van Londen",
      },
    },
    {
      id: uuidv4(),
      title: "Spoedeisende Hulp",
      start: "2020-11-18T13:00",
      end: "2020-11-18T14:00",
      extendedProps: {
        name: "Meis van Londen",
      },
    },
    {
      id: uuidv4(),
      title: "Urologie",
      start: "2020-11-19T13:00",
      end: "2020-11-19T16:00",
      extendedProps: {
        name: "Nagihan van Loo",
      },
    },
    {
      id: uuidv4(),
      title: "Urologische oncologie",
      start: "2020-11-22T11:00",
      end: "2020-11-22T16:00",
      extendedProps: {
        name: "Joao Mathijsen",
      },
    },
    {
      id: uuidv4(),
      title: "Verloskunde",
      start: "2020-11-23T09:00",
      end: "2020-11-23T10:00",
      extendedProps: {
        name: "Liset Noordenbos",
      },
    },
    {
      id: uuidv4(),
      title: "Urologie",
      start: "2020-11-25T10:00",
      end: "2020-11-25T17:00",
      extendedProps: {
        name: "Jerremy Gerssen",
      },
    },
  ];
}

export default data;
